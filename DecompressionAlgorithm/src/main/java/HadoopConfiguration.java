import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class HadoopConfiguration {

    private static Properties properties;

    public static void setup() {

        // Retrieves the file configuration.properties
        try (InputStream input = HadoopConfiguration.class
                .getClassLoader().getResourceAsStream("configuration.properties")) {

            // Declaration of a new Properties object
            properties = new Properties();

            // Check if there are errors
            if (input == null) {
                System.out.println("Unable to retrieves the configuration.properties file");
                return;
            }

            // Load a properties file from class path, inside static method
            properties.load(input);

        } catch (IOException ex) {
            System.out.println(" There are problem to retrieves the configuration.properties file!");
            ex.printStackTrace();
        }
    }

    public static String get(String propertyName) {
        return properties.getProperty(propertyName);
    }

}
