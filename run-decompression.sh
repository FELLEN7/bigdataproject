#!/bin/bash

HADOOP_HOME="/Users/valentina/Library/hadoop-2.9.2"
INPUT_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/InputsOutputs"
JAR_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/DecompressionAlgorithm/target"
PRJ_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject"
FORMATTING_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/Formatting"

echo "creating directory structure..."
$HADOOP_HOME/bin/hadoop dfs -mkdir /user
$HADOOP_HOME/bin/hadoop dfs -mkdir /user/valentina
$HADOOP_HOME/bin/hadoop dfs -mkdir /user/valentina/decompression

echo "delete file inside decompression folder on dfs"
$HADOOP_HOME/bin/hadoop dfs -rm -r /user/valentina/decompression/*

echo "Pre-processing the input..."
rm -rf $INPUT_DIR/cdeompression/decompressionInput.txt
python $FORMATTING_DIR/Pre_processing.py decompression/input.txt decompression/decompressionInput.txt

echo "copying input files to dfs..."
$HADOOP_HOME/bin/hadoop dfs -copyFromLocal $INPUT_DIR/decompression/* /user/valentina/decompression/

echo "running decompression..."
$HADOOP_HOME/bin/hadoop jar $JAR_DIR/decompression-algorithm-0.0.1-SNAPSHOT-shaded.jar

echo "Retrieving output..."
$PRJ_DIR/retrieve-decompression-output.sh

echo "Post-processing the input..."
rm -rf $INPUT_DIR/decompression/decompressedOutput.txt
python $FORMATTING_DIR/Post_processing.py decompression/decompressionOutput/part-r-00000 decompression/decompressedOutput.txt