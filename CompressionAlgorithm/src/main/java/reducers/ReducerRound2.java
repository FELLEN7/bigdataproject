package reducers;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.util.logging.Logger;

import java.io.*;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ReducerRound2 extends Reducer<Text,IntWritable,IntWritable,IntWritable>{

    // Declaration of the dictionary
    private HashMap<Text,Integer> dictionary= new HashMap<>();
    //private static final Logger sLogger = Logger.getLogger(ReducerRound2.class.getName());
    private static final Log sLogger = LogFactory.getLog(ReducerRound2.class);



    /**
     * The setup function is used to retrieves the output data of the previous round saved in the distributed cache.
     * @param context is the context where the reducer will works
     * @throws IOException
     * @throws InterruptedException
     */


    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        URI[] cacheFiles = context.getCacheFiles();

        if (cacheFiles != null && cacheFiles.length > 0)
        {
            try
            {
                BufferedReader reader = new BufferedReader(new FileReader("dictionary"));
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] split = line.split(" ");
                    String word = split[0];
                    String id = split[1];
                    sLogger.info(word);
                    sLogger.info(id);
                    dictionary.put(new Text(word),Integer.parseInt(id));
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            sLogger.error("no cache file found");
        }
    }


    /**
     * Reducer function.
     * @param key is a word
     * @param value is the list of ids given by the different mappers to the word used as key
     * @param context is the context of the job configuration where the reducer will works
     */
    public void reduce(Text key, Iterable<IntWritable> value, Context context) throws IOException, InterruptedException{

        // Find the wordId of the key(or word)
        Integer wordId = dictionary.get(key);


        // For each position where the word were int the original text, emit a pair <wordId, position>
        for (IntWritable position : value){
            context.write(new IntWritable(wordId), position);
        }



    }
}
