package mappers;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapRound2 extends Mapper<Object, Text, Text, IntWritable> {
    /**
     * Map function.
     *
     * @param key     is empty
     * @param value   is the block of the file
     * @param context is the context of the job configuration where the map will works
     */
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

        // Retrieve all the single words in the block input
        String[] wordsPositions = value.toString().split(" ");

        // for each pair word:position, emit word as key and position as value
        for (String wordPosition : wordsPositions) {
            String[] split = wordPosition.split(":");
            String word = split[0];

            if(split.length < 2 || word.equals(" ") || word.equals("")){
                continue;
            }

            int position = Integer.parseInt(split[1]);
            context.write(new Text(word), new IntWritable(position));
        }
    }

}
