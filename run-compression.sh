#!/bin/bash

HADOOP_HOME="/Users/valentina/Library/hadoop-2.9.2"
INPUT_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/InputsOutputs"
PRJ_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject"
JAR_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/CompressionAlgorithm/target"
FORMATTING_DIR="/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/Formatting"

echo "creating directory structure..."
$HADOOP_HOME/bin/hadoop dfs -mkdir /user
$HADOOP_HOME/bin/hadoop dfs -mkdir /user/valentina
$HADOOP_HOME/bin/hadoop dfs -mkdir /user/valentina/compression

echo "delete file inside compression folder on dfs"
$HADOOP_HOME/bin/hadoop dfs -rm -r /user/valentina/compression/*

echo "Pre-processing the input..."
rm -rf $INPUT_DIR/compression/compressionInput.txt
python $FORMATTING_DIR/Pre_processing.py compression/input.txt compression/compressionInput.txt

echo "copying input files to dfs..."
$HADOOP_HOME/bin/hadoop dfs -copyFromLocal $INPUT_DIR/compression/* /user/valentina/compression/

echo "running compression..."
$HADOOP_HOME/bin/hadoop jar $JAR_DIR/compression-algorithm-0.0.1-SNAPSHOT-shaded.jar

echo "Retrieving output..."
$PRJ_DIR/retrieve-compression-output.sh

echo "Post-processing the input..."
rm -rf $INPUT_DIR/compression/compressedOutput.txt
python $FORMATTING_DIR/Post_processing.py compression/compressionOutputRound2/part-r-00000 compression/compressedOutput.txt

echo "Copy the compressedOutput in decompression folder as decompressionInput.txt"
cp $INPUT_DIR/compression/compressedOutput.txt $INPUT_DIR/decompression/input.txt