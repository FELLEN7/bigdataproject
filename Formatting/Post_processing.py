# Module sys
import sys                
import os

source_path = str(sys.argv[1])
destination_path = str(sys.argv[2])

if(len(source_path) == 0 or len(destination_path) == 0):
        print("missing arguments")
        exit(1)

path= "/Users/valentina/Documents/University/NeDaS/Projects/bigdataproject/InputsOutputs/"

f = open(path + source_path, "r")
f1 = open(path + destination_path, "a+")

positions_wordIds = {}

# Add all the pairs <wordId/word, position> in the dictionary
for line in f:
    splittedStr = line.split(" ")
    code = splittedStr[0]
    pos = int(splittedStr[1])
    positions_wordIds[pos] = code

# Sort the dictionary by keys
sorted_keys = sorted(positions_wordIds.keys())

# Write the file as a text file without position
for position in sorted_keys:
    f1.write(positions_wordIds[position]+ " ")

f.close()
f1.close()

